# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard

_IMPORT_TYPES = []


class ImportInit(ModelView):
    'Import Init'
    _name = 'account.import.init'
    _description = __doc__

    type = fields.Selection(_IMPORT_TYPES, 'Import Type',
        required=True)

ImportInit()


class Import(Wizard):
    'Import'
    _name = 'account.import'

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'account.import.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('choice', 'Next', 'tryton-ok', True),
                ],
            },
        },
        'choice': {
            'result': {
                'type': 'choice',
                'next_state': '_choice',
            },
        },
        'demo': {
            'result': {
                'type': 'action',
                'action': '_action_import_demo',
                'state': 'end',
            },
        },
    }

    def _choice(self, data):
        return data['form']['type']

    def _action_import_demo(self, data):
        return {}

Import()
