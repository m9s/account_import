# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Import',
    'name_de_DE': 'Buchhaltung Import',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Account Import
    - Base module for different import methods for accounting

    ''',
    'description_de_DE': '''Buchhaltung Import
    - Basismodul für verschiedene Importmethoden für die Buchhaltung
    ''',
    'depends': [
        'account'
    ],
    'xml': [
        'account_import.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
